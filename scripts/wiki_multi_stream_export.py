"""
graphed_education
Copyright (C) 2023 LoveIsGrief

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <http://www.gnu.org/licenses/>.
"""
import abc
import argparse
import bz2
import csv
import json
import re
import typing
from contextlib import AbstractContextManager
from enum import Enum
from typing import (
    Any, Generator, Iterable, Iterator, Optional, Protocol, Set, Tuple,
)
from xml.etree import ElementTree as ET


class OutputFormat(Enum):
    CSV = "CSV"
    JSON = "JSON"


class CSVWriter(Protocol):
    """A helper for the missing type definition for the output of csv.writer()"""

    def writeheader(self):
        ...

    def writerow(self, row: Iterable[Any]) -> Any:
        ...

    def writerows(self, rows: Iterable[Iterable[Any]]) -> None:
        ...


class GraphWriter(AbstractContextManager):
    """
    Internal class to match with OutputFormat that handles writing a graph in a specific format
    """
    file: Optional[typing.IO]

    def __init__(self, path: str):
        self.path = path

    @abc.abstractmethod
    def write_nodes(self, target_node_name: str, source_nodes: Set[str]):
        """
        Given a target node and nodes directed at it, write to the target file
        """


class GraphCSVWriter(GraphWriter):
    """
    Writes out a graph in the format of a CSV.
    """
    writer: Optional[CSVWriter]

    COLUMNS = [
        "target node",
        "source node",
    ]

    def __init__(self, path: str):
        super().__init__(path)

    def __enter__(self):
        self.file = open(self.path, "w")
        self.writer = csv.writer(self.file)
        self.writer.writeheader()

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.file:
            self.file.close()
        self.writer = None

    def write_nodes(self, target_node_name: str, source_nodes: Set[str]):
        if not self.writer:
            return
        for link in source_nodes:
            self.writer.writerow([target_node_name, link])


class GraphJSONWriter(GraphWriter):
    """
    Writes a JSON list of nodes in the following format

        [
        ,{<target node>:[<source node1>,<source node2>,...,<source nodeN>]
        ...
        ]
    """
    first_data_written: bool

    def __init__(self, path: str):
        super().__init__(path)

    def __enter__(self):
        self.file = open(self.path, "w")
        # Open a JSON array
        self.file.writelines("[\n")
        self.first_data_written = False

    def __exit__(self, exc_type, exc_val, exc_tb):
        if self.file:
            # Close a JSON array
            self.file.write("]")
            self.file.close()

    def write_nodes(self, target_node_name: str, source_nodes: Set[str]):
        dump = json.dumps({target_node_name: sorted(source_nodes)})
        to_write = dump
        if self.first_data_written:
            to_write = "," + to_write
        self.file.write(to_write + "\n")
        self.first_data_written = True


def main(input_path: str, out_format: OutputFormat, output_path: str):
    fbz, iterator = open_xml(input_path)
    match out_format:
        case OutputFormat.CSV:
            writer = GraphCSVWriter(output_path)
        case OutputFormat.JSON:
            writer = GraphJSONWriter(output_path)
        case _:
            print(f"Unknown format: {out_format}")
            return

    print(f"Writing to {output_path}")
    with writer:
        for name, links in iter_xml(iterator):
            if links:
                writer.write_nodes(name, links)
                print(f".", end="")
    print("\nDone")


def open_xml(path: str) -> Tuple[bz2.BZ2File, Iterator[Tuple[str, ET.Element]]]:
    fbz = bz2.open(path)
    return fbz, ET.iterparse(fbz, ("start", "end"))


def iter_xml(parser_iter: Iterator[Tuple[str, ET.Element]]) -> Generator[
    Tuple[str, Set[str]], None, None
]:
    _, root = next(parser_iter)
    for (event, elem) in parser_iter:
        if event == "end" and elem.tag.endswith("}page"):
            if pair := get_pair(elem):
                yield pair
            root.clear()


def get_pair(page_elem: ET.Element) -> Optional[Tuple[str, Set[str]]]:
    links = set()
    # Redirect element = redirect page, which we can ignore
    if page_elem.find("{*}redirect") is not None:
        return

    # Without a title, there's no pair
    if (title_el := page_elem.find("{*}title")) is None:
        return
    name = title_el.text.strip().lower()
    if should_ignore_node(name):
        return

    # Without text,
    if (text_el := page_elem.find("{*}revision/{*}text")) is not None and text_el.text:
        links = parse_for_links(text_el.text)

    return name, links


LINK_RE = re.compile(
    r"\[\[("
    r"(?P<title>[\w\s]+)"
    r"|"
    r"((?P<real_title>[\w\s]+)\|(?P<custom_title>[\w\s]+))"
    r")\]\]")


def parse_for_links(text) -> Set[str]:
    links = set()
    for match in LINK_RE.finditer(text):
        groups = match.groupdict()
        link_title = groups.get("title") or groups.get("real_title")
        if link_title:
            lower_title = link_title.strip().lower()
            if should_ignore_node(link_title):
                continue
            links.add(lower_title)
    return links


def should_ignore_node(name: str) -> bool:
    """
    Certain pages aren't articles, but could contain other things like files, lists, etc.
    Such pages shouldn't be included in the output
    """
    return any([
        name.startswith("file:"),
        name.startswith("category:"),
        name.startswith("list of"),
    ])


if __name__ == '__main__':
    parser = argparse.ArgumentParser(
        description="Convert a wikipedia .xml.bz2 export into a directed graph of nodes."
                    "CSV: target_node, source_node\n"
                    "JSON: list of dicts. each dict has a single key (target_node) "
                    "with a list of source_nodes\n"
    )
    parser.add_argument(
        "-f", "--format",
        choices=[f.value for f in OutputFormat],
        default=OutputFormat.CSV,
        help="Output format"
    )
    parser.add_argument(
        "exported_file",
        help="The .xml.bz2 wikipedia export "
             "https://en.wikipedia.org/wiki/Wikipedia:Database_download"
    )
    parser.add_argument(
        "output_target",
        help="Filename for the output file"
    )

    args = parser.parse_args()
    main(
        args.exported_file,
        OutputFormat(args.format),
        args.output_target,
    )
