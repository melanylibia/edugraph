# EduGraph

A repo to gather thoughts, ideas and implementation ideas of a platform that uses graphs to find connections between educational topics.

## Goals

This project's absolute end goal is to foster curiosity and make learning, understanding and knowing a quintessential life goal of our species.
It's a lofty goal, but it's better to die trying than never try at all.

More specifically, this project aims to achieve this by answering two main questions people hopefully ask themselves

**What do I know and what can I do/learn with that?**

Everyone has found themselves bored but willing to do something productive, but stuck at the question of "what can I do/learn?".

**What do I need in order to learn what I want to learn?**

Everything we want to learn or understand has prerequisites of skills and/or knowledge.
Sometimes (if at all) it's only stated what we need to know or understand, but not at what level.
And if the level is stated, it's not universal what that level means. 
A diploma from one institution can be worth more or less than that of another institution.

--------------------

For more information, check out the [docs].

[docs]: https://namingthingsishard.gitlab.io/social/education/edugraph/
