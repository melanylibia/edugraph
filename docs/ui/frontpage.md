Frontpage
=========

When the user first arrives, they should be able to see what most people are interested in learning
for different time intervals aka trending topics.

![Frontpage](../_static/Graphed.drawio)
